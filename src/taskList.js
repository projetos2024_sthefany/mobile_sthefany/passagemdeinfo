import React from "react";
import { View, Text, TouchableOpacity, FlatList } from "react-native";
import { useNavigation } from "@react-navigation/native";

const TaskList = ({ navigation }) => {
  const tasks = [
    {
      id: 1,
      title: "Ir ao Mercado",
      date: "2024-02-27",
      time: "10:00",
      address: "Supermercado SS",
    },
    {
      id: 2,
      title: "Fazer exercícios",
      date: "2024-02-27",
      time: "15:00",
      address: "Academia Fitness",
    },
    {
      id: 3,
      title: "Ler um livro",
      date: "2024-02-28",
      time: "19:00",
      address: "Em casa",
    },
  ];
  const taskPress = (task) => {
    navigation.navigate("DetalhesDasTarefas", {task});
  };
  return (
    <View>
      <FlatList
        data={tasks}
        keyExtractor={(item) => item.id.toString}
        renderItem={({ item }) => (
          <TouchableOpacity 
          onPress={() => taskPress(item)}>
            <Text>{item.title}</Text>
          </TouchableOpacity>
        )}
      />
    </View>
  );
};
export default TaskList;
